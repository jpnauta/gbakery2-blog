# gbakery2-blog

Provides tools to display dynamic content from Glamorgan Bakery's API servers
onto static HTML pages.

## Installation

```
npm install
```

## Build

```
npm run build
```

## Usage

To use this project, simply inject `dist/bundled.js` into the page's HTML
and add the appropriate HTML ID, as desired.

```html
<!DOCTYPE html>
<html lang="en">
<head>
</head>
<body>

<p>
  <h4>
    Cake list
  </h4>
  <div id="cake-list"></div>
  </p>
  
  <p>

  <h4>
    Cake detail
  </h4>
  <div id="cake-detail" cake-slug="5-buttercream-transfer"></div>
</p>

<!-- Inject bundled script here -->
<script>...</script>

</body>
</html>
```

## Publishing

To publish your code to the live website:

1. Build the code using the `npm run build` command
2. Log onto [Squarespace](https://account.squarespace.com/)
3. Go to Settings > Advanced > Code Injection
4. Replace the `Footer` code with `<script>...</script>`, where `...` is the code from `dist/bundle.js`
5. Save your changes