module.exports = {
  app: {
    url: 'https://app.glamorganbakery.com'
  },
  templates: {
    cakesList: '<div style="color: red">{{name}}</div>',
    cakeDetail: '<div style="color: blue">{{name}}</div>'
  }
};
