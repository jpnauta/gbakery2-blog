/**
 * Simple templates for testing purposes
 */

export const templates = {
  cakesListWrapper: '<div>{{{content}}}</div>',
  cakesListItem: '<div style="color: red">{{name}}</div>',
  cakeDetail: '<div style="color: blue">{{name}}</div>'
};