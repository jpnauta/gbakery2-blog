/**
 * HTML templates for Squarequare's "Avenue" template
 * https://avenue-demo.squarespace.com/
 */

export const templates = {
  cakesListWrapper: `
    <form class="form-wrapper" method="GET">
      <div class="field-list">
        <div class="field" style="display: flex;">
          <input class="field-element" name="q" type="text" placeholder="Search" value="{{params.q}}">
          <input type="submit" value="Search">
        </div>
      </div>
    </form>
    <div id="projectThumbs">{{{content}}}</div>
    <div style="text-align: center; clear: both;">
      {{#page.previous_page_number}}
        <form class="form-wrapper" style="display: inline" method="GET">
          <input name="q" type="hidden" value="{{params.q}}">
          <input name="page" type="hidden" value="{{page.previous_page_number}}">
          <input class="button sqs-system-button sqs-editable-button" style="padding: 10px" type="submit" value="Previous">
        </form>
      {{/page.previous_page_number}}

      {{#page.next_page_number}}
        <form class="form-wrapper" style="display: inline" method="GET">
          <input name="q" type="hidden" value="{{params.q}}">
          <input name="page" type="hidden" value="{{page.next_page_number}}">
          <input class="button sqs-system-button sqs-editable-button" style="padding: 10px" type="submit" value="Next">
        </form>
      {{/page.next_page_number}}
    </div>
  `,
  cakesListItem: `
    <a class="project" href="/cake-detail/?slug={{slug}}">
      <div>
        <div class="project-image">
          <div class="intrinsic">
            <div class="content-fill" style="overflow: hidden;">
              <img style="height: 100%;" src="{{img_med_square}}">
            </div>
          </div>
        </div>
        <div class="project-title">
          {{name}} (#{{id}})
        </div>
      </div>
    </a>
  `,
  cakeDetail: `
  <div class="sqs-block html-block sqs-block-html">
    <div class="sqs-block-content">
      <h1 class="text-align-center">
        {{name}} (#{{id}})
      </h1>
    </div>
  </div>
  <div class="sqs-block image-block sqs-block-image">
    <div class="sqs-block-content">
      <img style="margin: auto; max-width: 100%" src="{{img_med}}">
    </div>
  </div>
  <div class="sqs-block image-block sqs-block-image" style="max-width: 500px; margin: auto;">
    <div class="sqs-block-content">
      {{#description}}
        {{{description}}}
      {{/description}}
      {{^description}}
        (No description)
      {{/description}}
    </div>
    <div style="text-align: center; margin: 30px;">
      <a class="button sqs-system-button sqs-editable-button" href="/request-cake/?id={{id}}">
        Request Cake
      </a>
    </div>
  </div>
`
};
