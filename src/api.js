import 'whatwg-fetch';
import restful, {fetchBackend} from 'restful.js';
import {CONFIG} from './config';

export const rootApi = restful(CONFIG.app.url, fetchBackend(fetch));
export const cakesApi = rootApi.custom('api/public/cakes');
