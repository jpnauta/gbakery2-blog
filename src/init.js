import {cakeDetail, cakesList} from './templates';
import {cakesApi} from './api';
import {getParams} from "./urls";
import {makeID} from "./util";

// Render cake list, if needed
$(document).ready(() => {
  const element = $('#cake-list');
  if (element.length) {
    const {page, q} = getParams();
    cakesApi.get({page_size: 15, page, q}).then((res) => {
      doneLoading();
      const page = res.body().data();
      element.html(cakesList(page));
    });
  }
});

// Render cake detail, if needed
$(document).ready(() => {
  const element = $('#cake-detail');
  if (element.length) {
    const {slug} = getParams();
    cakesApi.custom(slug).get().then((res) => {
      doneLoading();
      const cake = res.body().data();
      element.html(cakeDetail(cake));
    });
  }
});

const cakeNumberElement = $('[name="SQF_CAKE_NUMBER"]');

// If "request cake" page
if (cakeNumberElement.length) {
  // Set cake number after page load
  $(document).ready(() => {
    const {id} = getParams();
    cakeNumberElement.val(id);
  });
}

// Generate and hide "Subject" field - assumed to be first form field
for (const formElement of document.forms) {
  const titleElement = formElement.getElementsByClassName("title")[0];
  if (titleElement && titleElement.textContent.indexOf("Subject") >= 0) {
    const subjectElement = $(titleElement.parentElement);
    subjectElement.children('input').val(makeID());
    subjectElement.hide();
  }
}

const doneLoading = (cake) => {
  $('#loading').remove();
};
