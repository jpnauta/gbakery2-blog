import {render} from 'mustache';
import map from 'lodash/map';
import {CONFIG} from './config';
import {getParams} from "./urls";

export const cakesList = (page) => {
  const params = getParams();
  const content =  map(page.results, (cake) => {
    return render(CONFIG.templates.cakesListItem, cake);
  }).join('');

  return render(CONFIG.templates.cakesListWrapper, {content, page, params})
};

export const cakeDetail = (cake) => {
  return render(CONFIG.templates.cakeDetail, cake);
};
