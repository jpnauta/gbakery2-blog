import {templates} from './templates/avenue';

// Import settings into system
export const CONFIG = {
  app: {url: "https://app.glamorganbakery.com"},
  templates,
};
